# reveal-pages

Generates Gitlab compatible pages [Reveal.js](https://github.com/hakimel/reveal.js) presentation from markdown


## How to use it ?


### Standalone 

```console
docker run --rm -v "$(pwd):/project" savadenn/reveal-pages
```

### With Gitlab CI

Simply add this to your .gitlab-ci.yml

```yaml
pages:
  image: docker:stable
  stage: deploy
  script:
    - docker run --rm -v "${CI_PROJECT_DIR}:/project" savadenn/reveal-pages
  artifacts:
    paths:
    - public
  only:
    - master
```


## What does it do ?

1. Find all markdown files in project folder (no sub folder)
1. Generates at least 1 horizontal slide per file
    * Starting with README.md 
    * then by alphabetique order


Follows [markdown rules](https://github.com/hakimel/reveal.js#external-markdown) with default
* Horizontal Slide separator (default)
    ```markdown
    ---
    ```
* Vertical slide double line feeds
* Speaker notes
    ```markdown
    notes:
    some notes
    ```
      
notes:
like this it only appears in speaker view

---

## Advance usage : watch mode

```console
docker run --rm -it -v "$(pwd):/project" savadenn/reveal-pages --watch
```

*Beware of -it, otherwise you will not be able to kill with CTRL+C*

---

## Extend ?

1. Copy [index.html](https://github.com/hakimel/reveal.js/blob/master/index.html) to ```.base.html``` in project root folder
2. Modify as your need but keep those lines
    ```html
	<section>Slide 1</section>
    <section>Slide 2</section>
    ```
3. Run as normal
