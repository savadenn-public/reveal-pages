build:
	@docker build -t savadenn/reveal-pages .

run: build
	@docker run --rm -v "${shell pwd}:/project" savadenn/reveal-pages

watch: build
	@docker run --rm -it -v "${shell pwd}:/project" savadenn/reveal-pages --watch

dev: run
	@echo Starting web server http://localhost
	@docker run --rm --net=host -v "${shell pwd}/public:/usr/share/nginx/html" nginx
