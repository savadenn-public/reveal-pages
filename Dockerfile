FROM alpine:3.9

ENV REVEAL_VERISON=3.8.0
ENV PROJECT=/project
ENV PUBLIC=${PROJECT}/public
ENV BASEFILE=/reveal/index.html
ENV EXTEND=${PROJECT}/.base.html
ENV FLAG=/.flag

RUN apk add --no-cache bash

ADD entrypoint.sh /
ADD onChange.sh /

RUN mkdir /reveal
RUN wget https://github.com/hakimel/reveal.js/archive/${REVEAL_VERISON}.tar.gz -O - | tar -xz --strip 1 -C /reveal

WORKDIR ${PROJECT}

ENTRYPOINT ["/entrypoint.sh"]
CMD [""]
