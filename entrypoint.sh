#!/usr/bin/env bash
set -e
set -u

if [[ -f "${FLAG}" ]]; then
  # Already running, flagging rerun and exit
  echo 1 >"${FLAG}"
  exit
else
  echo 0 >"${FLAG}"
fi

if [[ -f "${EXTEND}" ]]; then
  BASEFILE=${EXTEND}
fi

init() {
  echo -e "\e[33mCleans up ${PUBLIC} folder"
  rm -rf "${PUBLIC}" || true
  mkdir -p "${PUBLIC}"

  echo "Copy reveal.js files"
  cp -fR /reveal/* "${PUBLIC}"
  echo -e "\e[39m"
}

generate() {
  sect=${PUBLIC}/.sections.temp
  line=${PUBLIC}/.line.temp
  for file in "${PROJECT}"/*.md; do
    cp -f "${file}" "${PUBLIC}"
    {
      echo '<section data-markdown="'"$(basename "${file}")"'" '
      echo 'data-separator="^\r?\n---$"'
      echo 'data-separator-vertical="$\r?\n\r?\n$"'
      echo 'data-separator-notes="^notes?:"'
      echo '></section>'
    } >"${line}"
    if [[ ${file^^} == "REAME.MD" ]]; then
      cat "${line}" "${sect}" >"${sect}.2"
      mv "${sect}.2" "${sect}"
    else
      cat "${line}" >>"${sect}"
    fi
  done
  rm -f "${line}"

  echo -e "\e[36mFind inclusion point"
  n=$(grep -ni '<section>Slide 1</section>' "${BASEFILE}" | cut -f1 -d:)
  reg='^[0-9]+$'
  if ! [[ "${n}" =~ ${reg} ]]; then
    echo "error: Not a number, inclusion line is not found, verify if <section>Slide 1</section> is still present." >&2
    exit 1
  fi

  echo "Splitting file"
  head=${PUBLIC}/.head.temp
  head -"$((n - 1))" "${BASEFILE}" >"${head}"

  tail=${PUBLIC}/.tail.temp
  tail +"$((n + 2))" "${BASEFILE}" >"${tail}"

  cat "${head}" "${sect}" "${tail}" >"${PUBLIC}"/index.html

  rm -f "${head}"
  rm -f "${sect}"
  rm -f "${tail}"

  chmod 777 -R "${PUBLIC}"
  echo -e "\e[39m"
}

# ------------------------------------------------
# Main
# ------------------------------------------------
if [[ "$1" != "--skip-init" ]]; then
  init
fi

generate

val=$(cat "${FLAG}")
rm -f "${FLAG}"
if [[ "${val}" == "1" ]]; then
  # Another run was register during execution
  /entrypoint.sh --skip-init
fi

if [[ "$1" == "--watch" ]]; then
  echo "Watching for changes..."
  /sbin/inotifyd /onChange.sh "${PROJECT}:cwMymnd"
fi
